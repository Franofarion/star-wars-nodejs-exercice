const { getScriptsOnlyArgument } = require('./helpers/args');
const { getPlanetsDiameterFromFilmId } = require('./services/films');

const main = async () => {
  try {
    const filmId = getScriptsOnlyArgument();
    const planetsDiameter = await getPlanetsDiameterFromFilmId(filmId);

    // eslint-disable-next-line no-console
    console.log(planetsDiameter);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.error(e);
  }
};

main();

const hasSpecificTerrain = (allTerrains, terrain) => {
  if (typeof allTerrains !== 'string' || typeof terrain !== 'string') {
    return false;
  }
  return allTerrains.toLowerCase().includes(terrain.toLowerCase());
};

const waterSurfaceAboveZero = (waterSurface) => {
  const intWaterSurface = parseInt(waterSurface, 10);
  if (isNaN(intWaterSurface)) {
    return false;
  }
  return intWaterSurface > 0;
};

module.exports = {
  hasSpecificTerrain,
  waterSurfaceAboveZero
};

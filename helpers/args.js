const _ = require('lodash');

module.exports.getScriptsOnlyArgument = () => {
  const [, , arg] = process.argv;
  if (_.isNil(arg)) {
    throw new Error('No argument provided.');
  }

  const intArg = parseInt(arg, 10);
  if (_.isNaN(intArg)) {
    throw new Error('The provided argument must be an int.');
  }

  return intArg;
};

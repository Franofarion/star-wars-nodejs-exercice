const _ = require('lodash');
const wretch = require('wretch');
const fetch = (...args) =>
  import('node-fetch').then(({ default: nodeFetch }) => nodeFetch(...args));


const errorBuilder = (code, err) => {
  throw new Error(
    JSON.stringify({
      code,
      err
    })
  );
};

module.exports.apiBuilder = (apiUrl) => {
  return wretch()
    // Setting up wretch with fetch
    // See wretch documentation https://www.npmjs.com/package/wretch
    .polyfills({
      fetch,
      FormData: require("form-data"),
      URLSearchParams: require("url").URLSearchParams
    })
    .url(apiUrl)
    .catcher(400, err => errorBuilder(400, err))
    .catcher(401, err => errorBuilder(401, err))
    .catcher(403, err => errorBuilder(403, err))
    .catcher(404, err => errorBuilder(404, err))
    .catcher(405, err => errorBuilder(405, err))
    .catcher(409, err => errorBuilder(409, err))
    .catcher(500, err => errorBuilder(500, err));
};

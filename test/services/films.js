const sinon = require('sinon');
const { expect } = require('chai');

const SwapiPlanetsApi = require('../../resources/SwapiPlanetsApi');
const SwapiFilmsApi = require('../../resources/SwapiFilmsApi');
const { getPlanetsDiameterFromFilmId } = require('../../services/films');

const filmMock = {
  planets: [
    'url1',
    'url2',
    'url3'
  ]
};

const planetsMock1 = {
  surface_water: 1,
  terrain: 'mountains',
  diameter: '10'
};
const planetsMock2 = {
  surface_water: 6,
  terrain: 'mountains',
  diameter: '30'
};
const planetsMock3 = {
  surface_water: 0,
  terrain: 'mountains',
  diameter: '30'
};

describe('Film Service', () => {
  beforeEach(() => {
    sinon.restore();
  });

  it('should compute diameter 40 succesfully', async () => {
    // GIVEN
    sinon.stub(SwapiFilmsApi, 'readById').returns(filmMock);
    sinon.stub(SwapiPlanetsApi, 'readWithCustomUrl')
      .onFirstCall().returns(planetsMock1)
      .onSecondCall().returns(planetsMock2)
      .onThirdCall().returns(planetsMock3);

    // WHEN
    const diameter = await getPlanetsDiameterFromFilmId(1);

    // THEN
    expect(diameter).to.equal(40);
  });

  it('should compute diameter 0 succesfully', async () => {
    // GIVEN
    sinon.stub(SwapiFilmsApi, 'readById').returns(filmMock);
    sinon.stub(SwapiPlanetsApi, 'readWithCustomUrl')
      .onFirstCall().returns(planetsMock3)
      .onSecondCall().returns(planetsMock3)
      .onThirdCall().returns(planetsMock3);

    // WHEN
    const diameter = await getPlanetsDiameterFromFilmId(1);

    // THEN
    expect(diameter).to.equal(0);
  });

  it('should throw an error if film planets is null', async () => {
    // GIVEN
    sinon.stub(SwapiFilmsApi, 'readById').returns({
      planets: null
    });

    // WHEN
    try {
      await getPlanetsDiameterFromFilmId(1);
    } catch (err) {
      // THEN
      expect(err).to.deep.include(new Error('No planets for the provided film.'));
    }
  });

  it('should throw an error if film planets is empty', async () => {
    // GIVEN
    sinon.stub(SwapiFilmsApi, 'readById').returns({
      planets: []
    });

    // WHEN
    try {
      await getPlanetsDiameterFromFilmId(1);
    } catch (err) {
      // THEN
      expect(err).to.deep.include(new Error('No planets for the provided film.'));
    }
  });
});

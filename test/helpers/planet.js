const { expect } = require('chai');
const { hasSpecificTerrain, waterSurfaceAboveZero } = require('../../helpers/planet');

const terrains = 'mountains, Desert, OCEAN, sKy';

describe('Planet Helper', () => {
  describe('hasSpecificTerrain', () => {
    it('should return true with correct inputs', async () => {
      expect(hasSpecificTerrain(terrains, 'MOUnTains')).to.eq(true);
      expect(hasSpecificTerrain(terrains, 'desert')).to.eq(true);
      expect(hasSpecificTerrain(terrains, 'OcEAN')).to.eq(true);
      expect(hasSpecificTerrain(terrains, 'sky')).to.eq(true);
    });

    it('should return false if terrain not included', async () => {
      expect(hasSpecificTerrain(terrains, 'volcan')).to.eq(false);
      expect(hasSpecificTerrain(terrains, 'dessert')).to.eq(false);
    });

    it('should return false if incorrect inputs', async () => {
      expect(hasSpecificTerrain([], 'volcan')).to.eq(false);
      expect(hasSpecificTerrain(terrains, () => { })).to.eq(false);
    });
  });

  describe('waterSurfaceAboveZero', () => {
    it('should return true with correct inputs', async () => {
      expect(waterSurfaceAboveZero(1)).to.eq(true);
      expect(waterSurfaceAboveZero(10)).to.eq(true);
    });

    it('should return false if terrain not included', async () => {
      expect(waterSurfaceAboveZero(0)).to.eq(false);
      expect(waterSurfaceAboveZero(-1)).to.eq(false);
    });

    it('should return false if incorrect inputs', async () => {
      expect(waterSurfaceAboveZero([])).to.eq(false);
      expect(waterSurfaceAboveZero(() => { })).to.eq(false);
    });
  });
});

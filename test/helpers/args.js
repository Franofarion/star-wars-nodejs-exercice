const { expect } = require('chai');
const mockArgv = require('mock-argv');
const { getScriptsOnlyArgument } = require('../../helpers/args');

describe('Args Helper', () => {
  describe('getScriptsOnlyArgument', () => {
    it('should return correct arg 1', async () => {
      // GIVEN
      mockArgv(['1'], () => {});
      // WHEN
      const arg = getScriptsOnlyArgument();
      // THEN
      expect(arg).to.eq(1);
    });

    it('should return correct arg 6', async () => {
      // GIVEN
      mockArgv(['6'], () => {});
      // WHEN
      const arg = getScriptsOnlyArgument();
      // THEN
      expect(arg).to.eq(6);
    });

    it('should throw an error if no argument provided', async () => {
      // GIVEN
      mockArgv([], () => {});
      // WHEN
      try {
        const arg = getScriptsOnlyArgument();
      } catch (err) {
        // THEN
        expect(err).to.deep.include(new Error('No argument provided.'));
      }
    });

    it('should throw an error if argument is incorrect', async () => {
      // GIVEN
      mockArgv(['string'], () => {});
      // WHEN
      try {
        const arg = getScriptsOnlyArgument();
      } catch (err) {
        // THEN
        expect(err).to.deep.include(new Error('The provided argument must be an int.'));
      }
    });
  });
});

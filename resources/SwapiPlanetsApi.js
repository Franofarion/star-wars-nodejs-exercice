const { apiBuilder } = require('../helpers/api');

const swapiPlanetsApi = apiBuilder('https://swapi.dev/api/planets');

module.exports = class SwapiPlanetsApi {
  static async readById(id) {
    return swapiPlanetsApi.url(`/${id}`).get().json();
  }

  static async readWithCustomUrl(url) {
    return apiBuilder(url).get().json();
  }
};

const { apiBuilder } = require('../helpers/api');

const swapiFilmsApi = apiBuilder('https://swapi.dev/api/films');

module.exports = class SwapiFilmsApi {
  static async readById(id) {
    return swapiFilmsApi.url(`/${id}`).get().json();
  }
};

const _ = require('lodash');

const SwapiPlanetsApi = require('../resources/SwapiPlanetsApi');
const SwapiFilmsApi = require('../resources/SwapiFilmsApi');
const {
  computeDiameterReduceMethod,
  hasSpecificTerrain,
  waterSurfaceAboveZero
} = require('../helpers/planet');

const getPlanetsDiameterFromFilmId = async (filmId) => {
  const { planets: planetsUrls } = await SwapiFilmsApi.readById(filmId);

  if (_.isNil(planetsUrls) || planetsUrls.length === 0) {
    throw new Error('No planets for the provided film.');
  }

  const planets = await Promise.all(
    _.map(planetsUrls, url => SwapiPlanetsApi.readWithCustomUrl(url))
  );

  const diameter = _.reduce(planets,
    (acc, planet) => {
      return hasSpecificTerrain(planet.terrain, 'mountains')
        && waterSurfaceAboveZero(planet.surface_water)
        ? acc + parseInt(planet.diameter, 10)
        : acc;
    }, 0);

  return diameter;
};

module.exports = {
  getPlanetsDiameterFromFilmId
};
